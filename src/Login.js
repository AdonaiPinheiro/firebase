import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Button, FlatList } from 'react-native';
import Sistema from './Sistema';

export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email:'',
      senha:''
    };

    console.disableYellowBox = true;

    this.logar = this.logar.bind(this);

    Sistema.logout();
  }

  logar() {

    Sistema.addAuthListener((user)=>{
        if(user) {
            Sistema.getUserInfo((info)=>{
                this.props.navigation.navigate('Home', {
                    nome:info.val().nome
                });
            });
            
        }
    })

    Sistema.logIn(this.state.email, this.state.senha)
        .catch((error)=>{
            alert(error.code);
        });
    
  }

  render() {
    return(

      <View style={ styles.container }>

        <Text> Email:</Text>
        <TextInput onChangeText={(email)=>this.setState({email})} style={styles.input} />
       
        <Text> Senha:</Text>
        <TextInput secureTextEntry={true} onChangeText={(senha)=>this.setState({senha})} style={styles.input} />

        <Button title='Logar' onPress={this.logar} />
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    margin:10
  },
  h1:{
    fontSize:26,
    textAlign:'center'
  },
  input:{
    borderWidth:1,
    borderColor:'#CCC',
    borderRadius:5,
    padding:5,
    margin:5,
  },
  addArea:{
    borderWidth:1,
    borderColor:'#000',
    padding:5
  }
});