import React, { Component } from 'react';
import { View, Text, StyleSheet, BackHandler } from 'react-native';
import Sistema from './Sistema';


export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
        nome:this.props.navigation.state.params.nome
    };

    console.disableYellowBox = true;

  }

  componentWillMount = () => {
    BackHandler.addEventListener('hardwareBackPress', () => true);
  }

  render() {
    return(

      <View style={ styles.container }>
        <Text>Olá, {this.state.nome}</Text>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    margin:10
  }
});