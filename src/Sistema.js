import firebase from './FirebaseConnection';

class Sistema {

    logout() {
        firebase.auth().signOut();
    }

    addAuthListener(callback) {
        firebase.auth().onAuthStateChanged(callback);
    }

    logIn(email, senha) {
        return firebase.auth().signInWithEmailAndPassword(email, senha);
    }

    getUserInfo(callback) {
        firebase
        .database()
        .ref('users')
        .child(firebase.auth().currentUser.uid)
        .once('value', callback);
    }

}

export default new Sistema();