import React, { Component } from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';

import Login from './src/Login';
import Home from './src/Home';

const Navigation = createStackNavigator({
  Login:{
    screen:Login,
    navigationOptions:{
      title:'Login'
    }
  },
  Home:{
    screen:Home,
    navigationOptions:{
      header:null,
      headerLeft:null
    }
  }
});

export default createAppContainer(Navigation);